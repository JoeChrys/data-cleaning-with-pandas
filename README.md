## Data Cleaning with Pandas

### Introduction
This repository was created with the aim of introducing people to the Data Science world. As part of the [Hacktoberfest](https://hacktoberfest.com/) monthly event and the in-person event organized by .fromScratch, attendees will be able to learn and, then, contribute. There are some datasets available in the `data` folder that need to be cleaned properly.

### Steps
The attendees must pick one dataset, clean it and document the process along the way. This can be done easily on Google Colab or Jupyter notebook. When that's done, they must open a merge request providing any needed information regarding their solution. The solution must be in a `.ipynb` file with the following template as a name: `{dataset_name}_{contributor_full_name}.ipynb`. For example, `weather_george_papadopoulos.ipynb`. The correct implementations will be included in the repository.

### Data

#### Religion
Contains information about the income of people that belong to various religions.

#### Billboard
Contains information about songs and their billboard rating each week.

#### Weather
Contains information about temperatures for each day.